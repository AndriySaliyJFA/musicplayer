package com.example.mplayer.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mplayer.R;
import com.example.mplayer.activities.musicFeedActivity.FeedFragment;
import com.example.mplayer.activities.pojo.Playlist;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.FormatUtil;
import com.example.mplayer.util.TrackUtil;

import java.util.ArrayList;
import java.util.List;

import static com.example.mplayer.util.Constants.TAG_CHAIN;

public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.TrackViewHolder> {

    public interface TrackClickListener {
        void onPlayClicked(int previousPosition, Playlist playlist);

        void onPauseClicked();

        void onResumeClicked();
    }

    private static final String TAG = "TrackListAdapter";

    private List<Track> mTrackItems = new ArrayList<>();
    private TrackClickListener mTrackClickListener = null;

    private int sCurrentItemPosition = -1;

    public TrackListAdapter(@Nullable List<Track> trackList) {
        mTrackItems.clear();
        if (trackList != null) {
            mTrackItems.addAll(trackList);
        }
    }

    @NonNull
    @Override
    public TrackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_track, parent, false);
        return new TrackViewHolder(view);
    }

    public void refreshData(@NonNull List<Track> trackItems) {
        Log.d(TAG, "refresh data,current pos=" + sCurrentItemPosition);
        sCurrentItemPosition = TrackUtil.findActiveItemPosition(trackItems);
        mTrackItems.clear();
        mTrackItems.addAll(trackItems);
        notifyDataSetChanged();
    }

    private void updateItemById(Track track) {
        int position = mTrackItems.indexOf(track);
        if (position != -1) {
            Log.d(TAG, "find track with this id,pos=" +
                    position + "state:" + track.getState());
            mTrackItems.set(position, track);
            notifyItemChanged(position);
        }
    }

    public void updateItem(Integer position) {
        if (!mTrackItems.isEmpty()) {
            mTrackItems.get(position).setState(Track.STATE_TYPES.STATE_IDLE);
        }
        notifyItemChanged(position);
    }

    @Override
    public void onBindViewHolder(@NonNull final TrackViewHolder holder, final int position) {
        holder.bind(mTrackItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mTrackItems.size();
    }

    public void setTrackClickListener(TrackClickListener listener) {
        mTrackClickListener = listener;
    }

    class TrackViewHolder extends RecyclerView.ViewHolder implements FeedFragment.TrackStateListener {
        private final TextView textTitle;
        private final TextView textDuration;
        private final TextView textUsername;

        private final ImageView imageThumb;
        private final ImageButton btnPlay;
        private final ImageButton btnMenu;

        TrackViewHolder(@NonNull View itemView) {
            super(itemView);
            FeedFragment.setTrackStateListener(this);

            textTitle = itemView.findViewById(R.id.item_tv_title);
            textDuration = itemView.findViewById(R.id.item_tv_duration);
            textUsername = itemView.findViewById(R.id.item_tv_username);

            imageThumb = itemView.findViewById(R.id.item_iv_thumb);

            btnMenu = itemView.findViewById(R.id.item_iv_points);
            btnMenu.setOnClickListener(buttonMenu ->
                    Toast.makeText(buttonMenu.getContext(), "context menu", Toast.LENGTH_SHORT)
                            .show());

            btnPlay = itemView.findViewById(R.id.item_ib_play);
            btnPlay.setOnClickListener(button -> {
                sCurrentItemPosition = getAdapterPosition();

                switch (mTrackItems.get(getAdapterPosition()).getState()) {
                    case Track.STATE_TYPES.STATE_PLAYING:
                        mTrackClickListener.onPauseClicked();
                        break;
                    case Track.STATE_TYPES.STATE_PAUSED:
                        mTrackClickListener.onResumeClicked();
                        break;
                }
            });

            itemView.setOnClickListener(item -> {
                if (getAdapterPosition() != sCurrentItemPosition) {
                    Log.d(TAG, "click on another track");
                    mTrackClickListener.onPlayClicked(sCurrentItemPosition,
                            new Playlist(mTrackItems, getAdapterPosition()));
                    sCurrentItemPosition = getAdapterPosition();
                }
            });
        }

        void bind(final Track item) {
            setUpButtonImage(item);

            btnPlay.setVisibility(item.isActive() ? View.VISIBLE : View.INVISIBLE);
            btnMenu.setVisibility(item.isActive() ? View.INVISIBLE : View.VISIBLE);

            textTitle.setText(item.getTitle());
            textDuration.setText(FormatUtil.formatTimeSecondsToString(item.getDuration()));
            textUsername.setText(item.getUser().getUsername());

            Glide.with(imageThumb.getContext()).load(item.getThumb()).into(imageThumb);
        }

        private void setUpButtonImage(Track item) {
            switch (item.getState()) {
                case Track.STATE_TYPES.STATE_PLAYING:
                case Track.STATE_TYPES.STATE_DOWNLOADED:
                    btnPlay.setEnabled(true);
                    btnPlay.setImageResource(R.drawable.pause);
                    break;
                case Track.STATE_TYPES.STATE_PAUSED:
                    btnPlay.setEnabled(true);
                    btnPlay.setImageResource(R.drawable.play);
                    break;
                case Track.STATE_TYPES.STATE_DOWNLOADING:
                    btnPlay.setEnabled(false);
                    btnPlay.setImageResource(R.drawable.progress_rotate);
                    break;
            }
        }

        /**
         * it method just receive commands from my UI-part,
         * it just use track.getState () for display needed view,
         *
         * @param track track with state for display,and id for search in list
         */
        @Override
        public void onTrackStateChanged(Track track) {
            Log.d(TAG_CHAIN,
                    "chain 6,from feed fragment to trackList adapter: "
                            + track.getState() + "-" +
                            track.getState());
            updateItemById(track);
            if(track.getState().equals(Track.STATE_TYPES.STATE_IDLE)){
                sCurrentItemPosition=-1;
            }
        }
    }
}