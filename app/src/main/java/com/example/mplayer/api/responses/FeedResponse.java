package com.example.mplayer.api.responses;

import com.example.mplayer.api.entities.Track;

import java.util.List;

public class FeedResponse {
    private List<Track> trackList;

    public FeedResponse(List<Track> trackList) {
        this.trackList = trackList;
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<Track> trackList) {
        this.trackList = trackList;
    }
}
