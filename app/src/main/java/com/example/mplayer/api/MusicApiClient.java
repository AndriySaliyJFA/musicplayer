package com.example.mplayer.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MusicApiClient {
    private static MusicApiService mInstance;
    private static final String BASE_URL = "https://api-v2.hearthis.at\\/";

    private MusicApiClient() {
    }

    public static MusicApiService getInstance() {
        HttpLoggingInterceptor mLogging = new HttpLoggingInterceptor();
        mLogging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        if (mInstance == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(mLogging)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            mInstance = retrofit.create(MusicApiService.class);
        }
        return mInstance;
    }
}