package com.example.mplayer.api;

import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.api.entities.Track;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MusicApiService {
    /**
     * @param type  popular or new( use constants)
     * @param page  page to show
     * @param count entries per page (max: 20)
     * @return list of popular/new tracks
     */
    @GET("feed")
    Call<List<Track>> getFeed(@Query("type") String type,
                              @Query("page") int page,
                              @Query("count") int count);

    /**
     * get history of user(song that he listened)
     *
     * @param page   page to show
     * @param count  entries per page (max: 20)
     * @param secret secret code of user(for identification user)
     * @param key    key of user(for identification user)
     * @return list of recently listened tracks
     */
    @GET("v2.2/htry")
    Call<List<Track>> getHistory(@Query("page") int page,
                                 @Query("count") int count,
                                 @Query("secret") String secret,
                                 @Query("key") String key);

    /**
     * return result of search,u can search different type of content
     *
     * @param searchQuery query which will searching
     * @param type        tracks (or empty) / user / playlists
     * @param page        page to show
     * @param count       entries per page (max: 20)
     * @return list of Track
     */
    @GET("search")
    Call<List<Track>> getSearchQuery(@Query("t") String searchQuery,
                                     @Query("type") String type,
                                     @Query("page") int page,
                                     @Query("count") int count);

    /**
     * get user tracks or playlist's
     *
     * @param username name of user
     * @param type     playlists or tracks
     * @return list of user tracks
     */
    @GET("{username}")
    Call<List<Track>> getUserTracks(@Path("username") String username,
                                    @Query("type") String type);

    @POST("login")
    Call<User> getUser(@Query("email") String email,
                       @Query("password") String password);
}