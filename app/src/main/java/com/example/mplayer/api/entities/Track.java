package com.example.mplayer.api.entities;

import android.view.View;

import androidx.annotation.StringDef;

import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.util.Constants;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.example.mplayer.api.entities.Track.STATE_TYPES.STATE_DOWNLOADED;
import static com.example.mplayer.api.entities.Track.STATE_TYPES.STATE_DOWNLOADING;
import static com.example.mplayer.api.entities.Track.STATE_TYPES.STATE_IDLE;
import static com.example.mplayer.api.entities.Track.STATE_TYPES.STATE_PAUSED;
import static com.example.mplayer.api.entities.Track.STATE_TYPES.STATE_PLAYING;

/**
 * we don't use 'mVariable' naming because this class is used by Gson
 * and i avoid creating @SerializedName annotation for each variable
 */
public class Track {

    private static final int NEW_TYPE = 0;
    private static final int POPULAR_TYPE = 1;

    private int id;
    private int duration;
    private int visibility = View.INVISIBLE;
    private Constants.FEED_TYPES mFeedType;

    private String username;
    private String description;
    private String title;
    private String thumb;
    private String uri;
    @SerializedName("stream_url")
    private String streamUrl;

    private User user;

    @StringDef({STATE_PLAYING, STATE_PAUSED, STATE_DOWNLOADING, STATE_IDLE, STATE_DOWNLOADED})
    public @interface STATE_TYPES {
        String STATE_IDLE = "state_idle";
        String STATE_PLAYING = "state_playing";
        String STATE_PAUSED = "state_paused";
        String STATE_DOWNLOADING = "state_downloading";
        String STATE_DOWNLOADED = "state_downloaded";
    }

    private String state = STATE_IDLE;
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Track() {
    }

    public Track(int id, int duration, String title, String thumb, String streamUrl) {
        this.id = id;
        this.duration = duration;
        this.title = title;
        this.thumb = thumb;
        this.streamUrl = streamUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {

        this.streamUrl = streamUrl;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public int getVisibility() {
        return visibility;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public int getFeedType() {
        switch (mFeedType) {
            case TYPE_NEW:
                return NEW_TYPE;
            case TYPE_POPULAR:
                return POPULAR_TYPE;
            default:
                throw new RuntimeException("no feedType");
        }
    }

    public void setFeedType(Constants.FEED_TYPES feedTypes) {
        mFeedType = feedTypes;
    }

    public void setFeedType(int feedTypes) {
        mFeedType = feedTypes == NEW_TYPE ?
                Constants.FEED_TYPES.TYPE_NEW : Constants.FEED_TYPES.TYPE_POPULAR;
    }

    public boolean isActive() {
        return !state.equals(STATE_IDLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return id == track.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @NotNull
    @Override
    public String toString() {
        return "Track{" +
                "id=" + id +
                ", duration=" + duration +
                ", title='" + title + '\'' +
                ", thumb='" + thumb + '\'' +
                ", streamUrl='" + streamUrl + '\'' +
                '}';
    }
}