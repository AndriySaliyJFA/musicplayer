package com.example.mplayer.activities;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.example.mplayer.BuildConfig;
import com.example.mplayer.R;
import com.google.firebase.FirebaseApp;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        boolean isDebugMode = BuildConfig.BUILD_TYPE.equals(getString(R.string.DEBUG_MODE));

        FirebaseApp.initializeApp(getApplicationContext());
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(isDebugMode).build())
                .build();
        Fabric.with(getApplicationContext(), crashlyticsKit);
    }
}
