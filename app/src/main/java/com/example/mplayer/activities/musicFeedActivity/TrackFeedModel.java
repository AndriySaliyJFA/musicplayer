package com.example.mplayer.activities.musicFeedActivity;

import android.util.Log;

import com.example.mplayer.R;
import com.example.mplayer.api.MusicApiClient;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class TrackFeedModel {

    private static final int TRIES = 1;
    private static final String TYPE_NEW = "new";
    private static final String TYPE_POPULAR = "popular";
    private static int mTries;
    private static ApiFeedListener mFeedListener;

    private static final String TAG = "TrackFeedModel";

    public interface ApiFeedListener {
        void onResponse(List<Track> tracks, Constants.FEED_TYPES type);

        void onFailure(int errorMsg, boolean toShow);

    }

    static void setOnFeedResponseListener(final ApiFeedListener feedListener) {
        if (mFeedListener == null) {
            mFeedListener = feedListener;
        }
    }

    static void clearFeedListener() {
        mFeedListener = null;
    }

    /**
     * server work bad,so every time i try to get response
     * in case of error or successful send message using ApiListener object
     * post in listener list of popular tracks
     */
    static void getNewTracks() {
        mTries = TRIES;
        MusicApiClient.getInstance()
                .getFeed(TYPE_NEW, 1, 10)
                .enqueue(new Callback<List<Track>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Track>> call,
                                           @NotNull Response<List<Track>> response) {
                        Log.d(TAG, "on response in getNewTrack");
                        if (response.isSuccessful()) {
                            if (response.body() != null &&
                                    response.body().size() != 0 &&
                                    response.body().get(0).getTitle().length() > 0) {
                                mFeedListener.onResponse(
                                        response.body(),
                                        Constants.FEED_TYPES.TYPE_NEW
                                );
                            } else {
                                if (mTries > 0) {
                                    getNewTracks();
                                    --mTries;
                                } else {
                                    mFeedListener.onFailure(R.string.error_no_such_user, true);
                                }
                            }
                        } else {
                            mFeedListener.onFailure(R.string.error_server_unavailable, true);
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Track>> call, @NotNull Throwable t) {
                        if (t instanceof IOException) {
                            Log.e(TAG, "this is an actual network failure :( ");
                        } else {
                            Log.e(TAG, "fatal issue with connection");
                        }
                        if (mTries > 0) {
                            Log.d(TAG, "try load feed again");
                            getNewTracks();
                            --mTries;
                        } else {
                            Log.d(TAG, "error,no more tries");
                            mFeedListener.onFailure(R.string.error_try_again, true);
                        }
                    }
                });
    }

    /**
     * server work bad,so every time i try to get response
     * in case of error or successful send message using ApiListener object
     * post in listener list of popular tracks
     */
    static void getPopularTracks() {
        mTries = TRIES;
        MusicApiClient.getInstance()
                .getFeed(TYPE_POPULAR, 1, 10)
                .enqueue(new Callback<List<Track>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<Track>> call,
                                           @NotNull Response<List<Track>> response) {
                        Log.d(TAG, "on response in getPopularTrack");
                        if (response.isSuccessful()) {
                            if (response.body() != null &&
                                    response.body().size() != 0 &&
                                    response.body().get(0).getTitle().length() > 0) {
                                mFeedListener.onResponse(
                                        response.body(),
                                        Constants.FEED_TYPES.TYPE_POPULAR
                                );
                            } else {
                                Log.e(TAG, "error ,body empty!");
                                if (mTries > 0) {
                                    getPopularTracks();
                                    --mTries;
                                } else {
                                    Log.e(TAG, "error no more tries");
                                    mFeedListener.onFailure(R.string.error_no_such_user, true);
                                }
                            }
                        } else {
                            Log.e(TAG, "error server don't work");
                            mFeedListener.onFailure(R.string.error_server_unavailable, true);
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<Track>> call, @NotNull Throwable t) {
                        if (t instanceof IOException) {
                            Log.e(TAG, "this is an actual network failure :( ");
                        } else {
                            Log.e(TAG, "fatal issue with connection");
                        }

                        if (mTries > 0) {
                            Log.e(TAG, "try to load again");
                            getPopularTracks();
                            --mTries;
                        } else {
                            Log.e(TAG, "error no more tries");
                            mFeedListener.onFailure(R.string.error_try_again, true);
                        }
                    }
                });
    }
}