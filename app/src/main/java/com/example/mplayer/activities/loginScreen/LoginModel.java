package com.example.mplayer.activities.loginScreen;

import com.example.mplayer.R;
import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.api.MusicApiClient;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class LoginModel {
    //server work bad,so i will always send request twice
    private static final int TRIES = 1;
    private static int mTries;
    private static ApiUserListener mUserListener;

    interface ApiUserListener {
        void onResponse(User user);

        void onFailure(int errorMsg, boolean toShow);
    }

    static void setOnUserResponseListener(final ApiUserListener userListener) {
        if (mUserListener == null) {
            mUserListener = userListener;
        }
    }

    static void clearUserListener() {
        mUserListener = null;
    }

    /**
     * method use retrofit to log in,server work bad,so every time i try to login twice
     * in case of error or successful send message using ApiListener object
     *
     * @param user user with email and password
     */
    static void getUser(final User user) {
        mTries = TRIES;
        MusicApiClient.getInstance().
                getUser(user.getEmail(), user.getPassword()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null &&
                            response.body().getUsername() != null &&
                            response.body().getUsername().length() > 0) {
                        mUserListener.onResponse(response.body());
                    } else {
                        if (mTries > 0) {
                            getUser(user);
                            --mTries;
                        } else
                            mUserListener.onFailure(R.string.error_no_such_user, true);
                    }
                } else {
                    mUserListener.onFailure((R.string.error_server_unavailable), true);
                }
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                if (mTries > 0) {
                    getUser(user);
                    --mTries;
                } else
                    mUserListener.onFailure(R.string.error_server_unavailable, true);
            }
        });
    }
}