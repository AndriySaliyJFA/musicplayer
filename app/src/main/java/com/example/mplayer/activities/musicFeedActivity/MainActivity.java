package com.example.mplayer.activities.musicFeedActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.example.mplayer.R;
import com.example.mplayer.activities.playerActivity.PlayerFragment;
import com.example.mplayer.activities.playerActivity.PlayerViewModel;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.services.MusicPlayerService;
import com.example.mplayer.services.MusicPlayerServiceManager;
import com.example.mplayer.util.Constants;
import com.example.mplayer.util.DownloadManager;
import com.example.mplayer.util.SharedPreferencesManager;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String FRAGMENT_TAG = "track_tag";
    private static final String KEY_TITLE = "title";
    private static final String TAG = "MainActivity";
    private static final String PLAYER_FRAGMENT_TAG = "player_fragment_tag";

    private Toolbar mToolbar;
    private DrawerLayout mDrawer;
    private FeedFragment mPopularFeedFragment = null;
    private FeedFragment mNewFeedFragment = null;
    private PlayerFragment mPlayerFragment = null;

    private ServiceConnection connection;
    private MusicPlayerService mService;
    private PlayerViewModel mPlayerViewModel;
    private FeedViewModel mFeedViewModel;

    private Constants.FEED_TYPES mCurrentTitle = Constants.FEED_TYPES.TYPE_NEW;

    @Override
    protected void onStart() {
        super.onStart();
        initializeMusicPlayerService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPlayerViewModel = ViewModelProviders.of(MainActivity.this).get(PlayerViewModel.class);
        mFeedViewModel = ViewModelProviders.of(MainActivity.this).get(FeedViewModel.class);

        if (savedInstanceState == null) {
            mNewFeedFragment = FeedFragment.newInstance(Constants.FEED_TYPES.TYPE_NEW);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mNewFeedFragment, FRAGMENT_TAG)
                    .commit();
        }

        initializeActionBar();
        initializeNavigationDrawer(mToolbar);
        initializeObservers();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState,mCurrentTitle:" + mCurrentTitle);
        outState.putSerializable(KEY_TITLE, mCurrentTitle);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentTitle = (Constants.FEED_TYPES) savedInstanceState.getSerializable(KEY_TITLE);

        Objects.requireNonNull(getSupportActionBar()).setTitle(getCurrentTitle());
    }

    private void initializeNavigationDrawer(@NonNull Toolbar toolbar) {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(MainActivity.this);
        View headerView = navigationView.getHeaderView(0);

        mDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.app_name, R.string.app_name);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        ImageView userThumb = headerView.findViewById(R.id.iv_user_thumb);
        Glide.with(this).load(
                SharedPreferencesManager.getInstance(this)
                        .loadUser()
                        .getThumb())
                .into(userThumb);

        TextView userName = headerView.findViewById(R.id.tv_username);
        userName.setText(SharedPreferencesManager.getInstance(this).loadUser().getUsername());
    }

    private void initializeObservers() {
        mFeedViewModel.getOnTrackStateChanged().observe(this, playlist -> {
            switch (playlist.getCurrentTrack().getState()) {
                case Track.STATE_TYPES.STATE_IDLE:
                    if (getSupportFragmentManager().findFragmentByTag(PLAYER_FRAGMENT_TAG) != null) {
                        getSupportFragmentManager().beginTransaction()
                                .remove(Objects.requireNonNull(
                                        getSupportFragmentManager().findFragmentByTag(PLAYER_FRAGMENT_TAG)
                                ))
                                .commit();
                    }
                    break;
                case Track.STATE_TYPES.STATE_DOWNLOADING:
                case Track.STATE_TYPES.STATE_PLAYING:
                    if (mPlayerFragment == null) {
                        mPlayerFragment = new PlayerFragment();
                    }
                    if (getSupportFragmentManager().findFragmentByTag(PLAYER_FRAGMENT_TAG) == null) {
                        getSupportFragmentManager().beginTransaction()
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .add(R.id.player_container, mPlayerFragment, PLAYER_FRAGMENT_TAG)
                                .commit();
                    }
                    break;
            }
            mPlayerViewModel.onTrackStateChanged(playlist);
        });

        mFeedViewModel.getSnackBarMessage().observe(this, message ->
                Snackbar.make(findViewById(R.id.container),
                        getText(message), Snackbar.LENGTH_LONG)
                        .show());
    }

    private void initializeActionBar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getCurrentTitle());

        Button buttonSearch = findViewById(R.id.btn_search);
        EditText textSearch = findViewById(R.id.ed_search);

        buttonSearch.setOnClickListener(view -> textSearch.setVisibility(
                textSearch.getVisibility() == View.VISIBLE ?
                        View.INVISIBLE :
                        View.VISIBLE
                )
        );
    }

    private void initializeMusicPlayerService() {
        connection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                Log.d(TAG, "on service connected");
                MusicPlayerService.LocalBinder binder = (MusicPlayerService.LocalBinder) service;
                mService = binder.getService();

                mFeedViewModel.setMusicPlayerServiceManager(MusicPlayerServiceManager.newInstance(
                        mService,
                        DownloadManager.getInstance(getApplicationContext()))
                );

                mPlayerViewModel.setMusicPlayServiceManager(MusicPlayerServiceManager.getInstance());

                if (mPlayerViewModel.getRestoredState()) {
                    if (mPlayerFragment == null) {
                        mPlayerFragment = new PlayerFragment();
                    }
                    if (getSupportFragmentManager().findFragmentByTag(PLAYER_FRAGMENT_TAG) == null) {
                        getSupportFragmentManager().beginTransaction()
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .add(R.id.player_container, mPlayerFragment, PLAYER_FRAGMENT_TAG)
                                .commit();
                    }
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                Log.d(TAG, "onServiceDisconnected: ");
            }
        };
        Intent intent = new Intent(this, MusicPlayerService.class);
        this.bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onNavigationItemSelected(@NotNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_new:
                if (mNewFeedFragment == null) {
                    mNewFeedFragment = FeedFragment.newInstance(Constants.FEED_TYPES.TYPE_NEW);
                }

                if (mCurrentTitle != Constants.FEED_TYPES.TYPE_NEW) {
                    mCurrentTitle = Constants.FEED_TYPES.TYPE_NEW;

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, mNewFeedFragment, FRAGMENT_TAG)
                            .commit();
                }
                break;
            case R.id.nav_popular:
                if (mPopularFeedFragment == null) {
                    mPopularFeedFragment = FeedFragment.newInstance(Constants.FEED_TYPES.TYPE_POPULAR);
                }

                if (mCurrentTitle != Constants.FEED_TYPES.TYPE_POPULAR) {
                    mCurrentTitle = Constants.FEED_TYPES.TYPE_POPULAR;

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, mPopularFeedFragment, FRAGMENT_TAG)
                            .commit();
                }
                break;
            default:
                Log.e(TAG, "Don't find such item in Drawer");
        }
        Objects.requireNonNull(getSupportActionBar()).setTitle(getCurrentTitle());
        mFeedViewModel.setCurrentFeedType(mCurrentTitle);
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private int getCurrentTitle() {
        switch (mCurrentTitle) {
            case TYPE_POPULAR:
                return R.string.title_popular;
            case TYPE_NEW:
                return R.string.title_new;
            default:
                Log.e(TAG, "getCurrentTitle: no such title");
                throw new RuntimeException("no such tab");
        }
    }
}