package com.example.mplayer.activities.loginScreen.entities;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * i don't use 'mVariable' naming because this class is used by Gson
 * and i avoid creating @SerializedName annotation for each variable
 */
public class User {
    private int id;

    private transient String email;
    private transient String password;

    private String username;
    private String caption;
    @SerializedName("thumb_url")
    private String thumb;
    @SerializedName("background_url")
    private String background;
    private String secret;
    private String key;

    public User() {
    }

    public User(String username){
        this.username=username;
    }
    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(int id, String username, String caption, String thumb, String background, String secret, String key) {
        this.id = id;
        this.username = username;
        this.caption = caption;
        this.thumb = thumb;
        this.background = background;
        this.secret = secret;
        this.key = key;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(username, user.username) &&
                Objects.equals(caption, user.caption) &&
                Objects.equals(thumb, user.thumb) &&
                Objects.equals(background, user.background) &&
                Objects.equals(secret, user.secret) &&
                Objects.equals(key, user.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, caption, thumb, background, secret, key);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", caption='" + caption + '\'' +
                ", thumb='" + thumb + '\'' +
                ", background='" + background + '\'' +
                ", secret='" + secret + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}