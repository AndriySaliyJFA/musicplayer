package com.example.mplayer.activities.loginScreen;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.mplayer.R;
import com.example.mplayer.activities.musicFeedActivity.MainActivity;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";
    private LoginFragmentViewModel mViewModel;

    private EditText mEmail;
    private EditText mPassword;
    private TextInputLayout mEmailError;
    private TextInputLayout mPasswordError;
    private Button mButtonLogin;
    private ProgressBar mProgressBar;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_screen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeViews(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(LoginFragmentViewModel.class);

        initializeTextWatchers();
        initializeLiveDataObservers();
    }

    private void initializeTextWatchers() {
        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mViewModel.setEmail(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.setEmail(editable.toString());
            }
        });

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mViewModel.setPassword(editable.toString());
            }
        });
    }

    private void initializeLiveDataObservers() {
        mViewModel.getEmailErrorLiveData().observe(getViewLifecycleOwner(),
                errorMsg -> mEmailError.setError(errorMsg == null ? null : getString(errorMsg)));

        mViewModel.getPasswordErrorLiveData().observe(getViewLifecycleOwner(),
                errorMsg -> mPasswordError.setError(errorMsg == null ? null : getString(errorMsg)));

        mViewModel.getIsBusyLiveData().observe(getViewLifecycleOwner(),
                busy -> setControlsEnabled(!busy));

        mViewModel.getIsLoggedLiveData().observe(getViewLifecycleOwner(),
                logged -> {
                    if (logged) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).finish();
                    }
                });

        mViewModel.getOnErrorLiveData().observe(getViewLifecycleOwner(),
                errorMsg -> Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show());

        mViewModel.getConnectionStateMonitor().observe(getViewLifecycleOwner()
                , isNetworkAvailable -> {
                    if (!isNetworkAvailable) {
                        Toast.makeText(getActivity(),
                                getString(R.string.errorNoNetwork),
                                Toast.LENGTH_SHORT).show();
                    }
                    setControlsEnabled(isNetworkAvailable);
                });
    }

    private void setControlsEnabled(Boolean available) {
        mButtonLogin.setEnabled(available);
        mProgressBar.setVisibility(!available ? View.VISIBLE : View.INVISIBLE);
    }

    private void initializeViews(View view) {
        mEmail = view.findViewById(R.id.ed_email);
        mPassword = view.findViewById(R.id.ed_password);
        mEmailError = view.findViewById(R.id.til_email_layout);
        mPasswordError = view.findViewById(R.id.til_password_layout);
        mProgressBar = view.findViewById(R.id.pb_login);

        mButtonLogin = view.findViewById(R.id.btn_login);
        mButtonLogin.setOnClickListener(buttonLogin -> mViewModel.onLoginClicked());
    }
}