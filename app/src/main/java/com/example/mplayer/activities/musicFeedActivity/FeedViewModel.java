package com.example.mplayer.activities.musicFeedActivity;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mplayer.R;
import com.example.mplayer.activities.pojo.Playlist;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.database.DatabaseManager;
import com.example.mplayer.services.MusicPlayerServiceManager;
import com.example.mplayer.util.ConnectionStateMonitor;
import com.example.mplayer.util.Constants;
import com.example.mplayer.util.SingleLiveEvent;

import java.util.ArrayList;
import java.util.List;

public class FeedViewModel extends AndroidViewModel {

    private static final String TAG = "FeedViewModel";
    private static final int MAX_DURATION_SECONDS = 500;

    private int mPreviousPosition;

    private List<Track> mSavedNewTracks = new ArrayList<>();
    private List<Track> mSavedPopularTracks = new ArrayList<>();

    private MutableLiveData<Integer> mSnackBarMessage = new SingleLiveEvent<>();
    private MutableLiveData<Integer> mErrorMessage = new SingleLiveEvent<>();
    private MutableLiveData<List<Track>> mCurrentTracks = new MutableLiveData<>();
    private MutableLiveData<Playlist> mOnStateChanged = new MutableLiveData<>();

    private MutableLiveData<Integer> mResetItemPosition = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsRefreshingAvailable = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsDataLoading = new MutableLiveData<>();

    private ConnectionStateMonitor mConnectionStateMonitor;
    private Constants.FEED_TYPES mCurrentFeedType = Constants.FEED_TYPES.TYPE_NEW;

    private DatabaseManager mDatabaseManager;
    private MusicPlayerServiceManager mMusicPlayerServiceManager;

    private final MusicPlayerServiceManager.ServiceStateListener serviceStateListener =
            new MusicPlayerServiceManager.ServiceStateListener() {
                @Override
                public void onStateChanged(Playlist playlist) {
                    Log.d(Constants.TAG_CHAIN,
                            " chain 4,from serviceManager to feed ViewModel onStateChanged");
                    mPreviousPosition = playlist.getPreviousPosition();
                    if (mPreviousPosition != -1) {
                        mResetItemPosition.postValue(mPreviousPosition);
                    }
                    if(playlist.getCurrentTrack().getState().equals(Track.STATE_TYPES.STATE_IDLE)){
                        mResetItemPosition.postValue(playlist.getCurrentPosition());
                    }
                    mOnStateChanged.postValue(playlist);
                }

                @Override
                public void onServiceError(int messageStringId) {
                    mErrorMessage.postValue(messageStringId);
                }
            };

    private final TrackFeedModel.ApiFeedListener mOnFeedResponseListener =
            new TrackFeedModel.ApiFeedListener() {
                @Override
                public void onResponse(List<Track> tracks, Constants.FEED_TYPES feedType) {
                    Log.d(TAG, "feed received,count of tracks:" + tracks.size());

                    List<Track> filtratedTracks=removeTooLong(tracks);

                    updateCurrentTrackState(filtratedTracks);
                    updateTracks(feedType, filtratedTracks);
                    saveFeedInDatabase(filtratedTracks, feedType);
                }

                @Override
                public void onFailure(int errorMsg, boolean toShow) {
                    if (toShow) {
                        mErrorMessage.setValue(errorMsg);
                    }
                    mIsDataLoading.setValue(false);
                }
            };

    private List<Track> removeTooLong(List<Track> tracks) {
        List<Track> result = new ArrayList<>();
        result.clear();
        for (Track track : tracks) {
            if (track.getDuration() <= FeedViewModel.MAX_DURATION_SECONDS) {
                result.add(track);
            }
        }
        return result;
    }

    public FeedViewModel(@NonNull Application application) {
        super(application);

        TrackFeedModel.setOnFeedResponseListener(mOnFeedResponseListener);

        mConnectionStateMonitor = new ConnectionStateMonitor(getApplication());
        mDatabaseManager = DatabaseManager.getInstance(getApplication());

        boolean isActive = mConnectionStateMonitor.isNetworkAvailable();
        if (!isActive) {
            mIsRefreshingAvailable.setValue(false);
            mSnackBarMessage.postValue(
                    R.string.error_no_internet
            );
            onFeedUpdate(false);
        }
        initializeObservers();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        TrackFeedModel.clearFeedListener();
    }

    private void updateTracks(Constants.FEED_TYPES feedType, List<Track> tracks) {
        switch (feedType) {
            case TYPE_NEW:
                updateForNewTracks(tracks);
                mIsDataLoading.setValue(false);
                break;
            case TYPE_POPULAR:
                updateForPopularTracks(tracks);
                mIsDataLoading.setValue(false);
                break;
        }
        mMusicPlayerServiceManager.updateSavedData(tracks);
    }

    private void updateForNewTracks(List<Track> tracks) {
        mSavedNewTracks.clear();
        mSavedNewTracks.addAll(tracks);
        mCurrentTracks.setValue(mSavedNewTracks);
    }

    private void updateForPopularTracks(List<Track> tracks) {
        mSavedPopularTracks.clear();
        mSavedPopularTracks.addAll(tracks);
        mCurrentTracks.setValue(mSavedPopularTracks);
    }

    private void initializeObservers() {
        mConnectionStateMonitor.getConnectionState().observeForever(isConnectionAvailable -> {
            Log.d(TAG, "received internet status:" + isConnectionAvailable);
            mIsRefreshingAvailable.setValue(isConnectionAvailable);
            mSnackBarMessage.postValue(
                    isConnectionAvailable ?
                            R.string.connected :
                            R.string.error_no_internet
            );
            onFeedUpdate(isConnectionAvailable);
        });
    }

    private void onFeedUpdate(boolean isConnected) {
        if (!isFeedLoaded()) {
            if (isConnected) {
                feedPulledToRefresh();
            } else {
                mIsDataLoading.setValue(true);
                loadFeedFromDatabase(Constants.FEED_TYPES.TYPE_NEW);
                loadFeedFromDatabase(Constants.FEED_TYPES.TYPE_POPULAR);
            }
        }
    }

    private void saveFeedInDatabase(List<Track> tracks, Constants.FEED_TYPES feedType) {
        mDatabaseManager.saveFeedInDatabase(tracks, feedType);
    }

    private void loadFeedFromDatabase(Constants.FEED_TYPES feedType) {
        switch (feedType) {
            case TYPE_NEW:
                mSavedNewTracks.clear();
                mSavedNewTracks.addAll(mDatabaseManager.loadFeedFromDatabase(feedType));
                break;
            case TYPE_POPULAR:
                mSavedPopularTracks.clear();
                mSavedPopularTracks.addAll(mDatabaseManager.loadFeedFromDatabase(feedType));
                break;
        }
        mIsDataLoading.postValue(false);
        setSavedTracks();
    }

    /**
     * search is new data set has track that was active,
     * if it has,that put in track list saved track state
     *
     * @param tracks track list from response
     */
    private void updateCurrentTrackState(List<Track> tracks) {
        int position;
        if (mMusicPlayerServiceManager != null) {
            Playlist playlist = mMusicPlayerServiceManager.getSavedData();
            if (!playlist.isEmpty() && playlist.getCurrentPosition() != -1) {
                position = tracks.indexOf(playlist.getCurrentTrack());
            } else {
                position = -1;
            }
            if (position != -1) {
                tracks.set(position, playlist.getCurrentTrack());

                mPreviousPosition = playlist.getPreviousPosition();
                if (mPreviousPosition != -1)
                    mResetItemPosition.setValue(mPreviousPosition);
            }
        }
    }

    void feedPulledToRefresh() {
        switch (mCurrentFeedType) {
            case TYPE_NEW:
                refreshNewTracks();
                break;
            case TYPE_POPULAR:
                refreshPopularTracks();
                break;
        }
    }

    private void feedTypeChanged() {
        if (isFeedLoaded()) {
            setSavedTracks();
        } else {
            feedPulledToRefresh();
        }
    }

    private void setSavedTracks() {
        switch (mCurrentFeedType) {
            case TYPE_NEW:
                if (mSavedNewTracks.size() > 0) {
                    mCurrentTracks.postValue(mSavedNewTracks);
                    updateCurrentTrackState(mSavedNewTracks);
                } else {
                    feedPulledToRefresh();
                }
                break;
            case TYPE_POPULAR:
                if (mSavedPopularTracks.size() > 0) {
                    mCurrentTracks.postValue(mSavedPopularTracks);
                    updateCurrentTrackState(mSavedPopularTracks);
                } else {
                    feedPulledToRefresh();
                }
                break;
        }

    }

    private void refreshNewTracks() {
        mIsDataLoading.setValue(true);
        mCurrentTracks.setValue(mSavedNewTracks);
        TrackFeedModel.getNewTracks();
    }

    private void refreshPopularTracks() {
        mIsDataLoading.setValue(true);
        mCurrentTracks.setValue(mSavedPopularTracks);
        TrackFeedModel.getPopularTracks();
    }

    private boolean isFeedLoaded() {
        switch (mCurrentFeedType) {
            case TYPE_NEW:
                return mSavedNewTracks.size() > 0;
            case TYPE_POPULAR:
                return mSavedPopularTracks.size() > 0;
            default:
                return false;
        }
    }

    void setCurrentFeedType(Constants.FEED_TYPES type) {
        mCurrentFeedType = type;
        feedTypeChanged();
    }

    void setMusicPlayerServiceManager(MusicPlayerServiceManager serviceManager) {
        mMusicPlayerServiceManager = serviceManager;
        mMusicPlayerServiceManager.setServiceStateListener(serviceStateListener);
    }

    void onStartPlayClicked(Playlist playlist) {
        Log.d(Constants.TAG_CHAIN, "chain 2,from feed fragment to ViewModel onStartPlayClicked: ");
        mMusicPlayerServiceManager.startPlay(playlist);
    }

    void onPausePlayClicked() {
        mMusicPlayerServiceManager.pausePlay();
    }

    void onResumePlayClicked() {
        mMusicPlayerServiceManager.resumePlay();
    }

    LiveData<Playlist> getOnTrackStateChanged() {
        return mOnStateChanged;
    }

    LiveData<List<Track>> getTracksFeed() {
        return mCurrentTracks;
    }

    LiveData<Boolean> getIsDataLoadingLiveData() {
        return mIsDataLoading;
    }

    LiveData<Integer> getErrorMessage() {
        return mErrorMessage;
    }

    LiveData<Integer> getResetItemPosition() {
        return mResetItemPosition;
    }

    LiveData<Boolean> getIsRefreshingAvailable() {
        return mIsRefreshingAvailable;
    }

    LiveData<Integer> getSnackBarMessage() {
        return mSnackBarMessage;
    }
}