package com.example.mplayer.activities.pojo;

import com.example.mplayer.api.entities.Track;

import java.util.ArrayList;
import java.util.List;

public class Playlist {

    private List<Track> mTracks = new ArrayList<>();
    private int mCurrentPosition;

    public Playlist() {
    }

    public Playlist(List<Track> tracks, int currentPosition) {
        mTracks = new ArrayList<>();
        mTracks.addAll(tracks);
        mCurrentPosition = currentPosition;
    }

    public boolean isPlaying() {
        return mTracks.get(mCurrentPosition).getState().equals(Track.STATE_TYPES.STATE_PLAYING);
    }

    public void nextTrack() {
        if (mCurrentPosition < mTracks.size() - 1) {
            ++mCurrentPosition;
        }
    }

    public void previousTrack() {
        if (mCurrentPosition >= 1) {
            --mCurrentPosition;
        }
    }

    public Track getCurrentTrack() {
        if (mCurrentPosition < 0) {
            return mTracks.get(getPreviousPosition());
        }
        return mTracks.get(mCurrentPosition);
    }

    public Playlist setCurrentTrack(Track currentTrack) {
        int position = mTracks.indexOf(currentTrack);
        if (position != -1) {
            mTracks.set(position, currentTrack);
            mCurrentPosition = position;
        }
        return this;
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public int getPreviousPosition() {
        for (int i = 0; i < mTracks.size(); i++) {
            if (!mTracks.get(i).getState().equals(Track.STATE_TYPES.STATE_IDLE)) {
                if (i != mCurrentPosition) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean isEmpty() {
        return mTracks.size() < 1;
    }
}