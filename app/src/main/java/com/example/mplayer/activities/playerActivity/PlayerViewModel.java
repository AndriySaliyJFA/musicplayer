package com.example.mplayer.activities.playerActivity;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mplayer.activities.pojo.Playlist;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.services.MusicPlayerServiceManager;

public class PlayerViewModel extends AndroidViewModel {

    private static final String TAG = "PlayerViewModel";

    private MutableLiveData<Playlist> mPlaylistState = new MutableLiveData<>();

    private MusicPlayerServiceManager mMusicPlayerServiceManager;
    private Playlist mSavedPlaylist;

    public PlayerViewModel(@NonNull Application application) {
        super(application);
    }

    public void onTrackStateChanged(Playlist playlist) {
        mSavedPlaylist = playlist;
        mPlaylistState.postValue(mSavedPlaylist);
    }

    void onPlayClicked() {
        if (mSavedPlaylist.isPlaying()) {
            mMusicPlayerServiceManager.pausePlay();
        } else {
            mMusicPlayerServiceManager.resumePlay();
        }
    }

    void onPreviousClicked() {
        try {
            mSavedPlaylist.previousTrack();

        } catch (Exception e) {
            Log.e(TAG, "onNextClicked, but no next track ");
            e.printStackTrace();
        }
        mMusicPlayerServiceManager.startPlay(mSavedPlaylist);
    }

    void onNextClicked() {
        try {

            mSavedPlaylist.nextTrack();
        } catch (Exception e) {
            Log.e(TAG, "onNextClicked, but no next track ");
            e.printStackTrace();
        }
        mMusicPlayerServiceManager.startPlay(mSavedPlaylist);
    }

    public void setMusicPlayServiceManager(MusicPlayerServiceManager instance) {
        mMusicPlayerServiceManager = instance;
    }

    LiveData<Playlist> getTrackStateLiveData() {
        return mPlaylistState;
    }

    public boolean getRestoredState() {
        if (!mMusicPlayerServiceManager.getSavedData().isEmpty() &&
                mMusicPlayerServiceManager.getSavedData().getCurrentPosition() != -1) {
            mSavedPlaylist = mMusicPlayerServiceManager.getSavedData();
            mPlaylistState.postValue(mSavedPlaylist);
            return mMusicPlayerServiceManager.getSavedData().isPlaying();
        } else {
            return false;
        }
    }

    void onProgressChanged(int newProgress) {
        mMusicPlayerServiceManager.onProgressChanged(newProgress);
    }

     Track getCurrentTrack() {
        return mSavedPlaylist.getCurrentTrack();
    }
}