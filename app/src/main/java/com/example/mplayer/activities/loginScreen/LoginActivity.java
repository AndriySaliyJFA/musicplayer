package com.example.mplayer.activities.loginScreen;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.mplayer.R;

public class LoginActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG = "loginFragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.login_container, new LoginFragment(), FRAGMENT_TAG)
                    .commit();
        }
    }
}