package com.example.mplayer.activities.playerActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.example.mplayer.R;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.views.MusicProgressView;

import java.util.Objects;

public class PlayerFragment extends Fragment {

    private ImageButton mButtonPlay;
    private ImageButton mButtonPlayBottom;

    private ImageView mBigThumb;
    private ImageView mThumb;
    private TextView mTextTrackName;

    private MusicProgressView mMusicProgressView;

    private PlayerViewModel mPlayerViewModel;

    private boolean mIsUpdating = false;
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mIsUpdating) {
                mMusicProgressView.updateProgressBy(1);
            }
            mHandler.postDelayed(this, 1000);
        }
    };

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.music_player, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initializeControlsButtons(view);

        mThumb = view.findViewById(R.id.iv_player_thumb);
        mBigThumb = view.findViewById(R.id.iv_player_big_thumb);
        mTextTrackName = view.findViewById(R.id.tv_player_track_name);

        mMusicProgressView = view.findViewById(R.id.musicProgressView);
        mMusicProgressView.setOnProgressChangeListener(newProgress ->
                mPlayerViewModel.onProgressChanged(newProgress));

        LinearLayout linearLayout = view.findViewById(R.id.player_main_container);
        linearLayout.setOnClickListener(container -> {
            //HACK ,because elevation doesn't work for this layout and
            // i should prevent avoid clicks of background layout
        });
        super.onViewCreated(view, savedInstanceState);
    }

    private void initializeControlsButtons(View view) {
        ImageButton buttonNext = view.findViewById(R.id.btn_player_next);
        ImageButton buttonPrevious = view.findViewById(R.id.btn_player_prev);
        ImageButton buttonNextBottom = view.findViewById(R.id.btn_player_next_bottom);
        ImageButton buttonPreviousBottom = view.findViewById(R.id.btn_player_prev_bottom);

        buttonPrevious.setOnClickListener(btn -> mPlayerViewModel.onPreviousClicked());
        buttonNext.setOnClickListener(btn -> mPlayerViewModel.onNextClicked());
        buttonNextBottom.setOnClickListener(btn -> {
            mPlayerViewModel.onNextClicked();
            mMusicProgressView.dropProgress();
        });
        buttonPreviousBottom.setOnClickListener(btn -> {
            mPlayerViewModel.onPreviousClicked();
            mMusicProgressView.dropProgress();
        });

        mButtonPlay = view.findViewById(R.id.btn_player_play);
        mButtonPlayBottom = view.findViewById(R.id.btn_player_play_bottom);

        mButtonPlay.setOnClickListener(btn -> mPlayerViewModel.onPlayClicked());
        mButtonPlayBottom.setOnClickListener(btn -> mPlayerViewModel.onPlayClicked());

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPlayerViewModel = ViewModelProviders.of(getActivity()).get(PlayerViewModel.class);
        mMusicProgressView.restoreState(mPlayerViewModel.getCurrentTrack());
        initializeObservers();
    }

    private void initializeObservers() {
        mPlayerViewModel.getTrackStateLiveData().observe(getViewLifecycleOwner(), playlist -> {
            switch (playlist.getCurrentTrack().getState()) {
                case Track.STATE_TYPES.STATE_PLAYING:
                    mButtonPlay.setImageResource(R.drawable.pause);
                    mButtonPlayBottom.setImageResource(R.drawable.pause);

                    startProgressUpdating();
                    break;
                case Track.STATE_TYPES.STATE_DOWNLOADED:
                    mButtonPlay.setImageResource(R.drawable.pause);
                    mButtonPlayBottom.setImageResource(R.drawable.pause);
                    mMusicProgressView.changeTrack(playlist.getCurrentTrack());

                    startProgressUpdating();
                    break;
                case Track.STATE_TYPES.STATE_PAUSED:
                    mButtonPlay.setImageResource(R.drawable.play);
                    mButtonPlayBottom.setImageResource(R.drawable.play);

                    pauseProgressUpdating();
                    break;
                case Track.STATE_TYPES.STATE_DOWNLOADING:
                    pauseProgressUpdating();
                    mButtonPlay.setImageResource(R.drawable.progress_rotate);
                    mButtonPlayBottom.setImageResource(R.drawable.progress_rotate);
                    break;
            }
            updateUI(playlist.getCurrentTrack());
        });
    }

    private void pauseProgressUpdating() {
        mHandler.removeCallbacks(mRunnable);
        mIsUpdating = false;
    }

    private void startProgressUpdating() {
        mHandler.removeCallbacks(mRunnable);
        mIsUpdating = true;
        Objects.requireNonNull(getActivity()).runOnUiThread(mRunnable);
    }

    private void updateUI(Track track) {
        Glide.with(Objects.requireNonNull(getActivity()))
                .load(track.getThumb())
                .into(mThumb);

        Glide.with(Objects.requireNonNull(getActivity()))
                .load(track.getThumb())
                .into(mBigThumb);

        mTextTrackName.setText(track.getTitle());
    }
}