package com.example.mplayer.activities.loginScreen;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mplayer.R;
import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.util.ConnectionStateMonitor;
import com.example.mplayer.util.SharedPreferencesManager;
import com.example.mplayer.util.SingleLiveEvent;
import com.example.mplayer.util.ValidateUtil;

public class LoginFragmentViewModel extends AndroidViewModel implements LoginModel.ApiUserListener {

    private static final String TAG = "LoginViewModel";
    private final SharedPreferencesManager mSharedPreferenceManager =
            SharedPreferencesManager.getInstance(getApplication());

    private MutableLiveData<Integer> mEmailError = new MutableLiveData<>();
    private MutableLiveData<Integer> mPasswordError = new MutableLiveData<>();
    private MutableLiveData<String> mEmail = new MutableLiveData<>();
    private MutableLiveData<String> mPassword = new MutableLiveData<>();
    private MutableLiveData<Boolean> mInProgress = new SingleLiveEvent<>();
    private MutableLiveData<Boolean> mIsLogged = new SingleLiveEvent<>();
    private MutableLiveData<Integer> mOnError = new SingleLiveEvent<>();
    private ConnectionStateMonitor connectionStateMonitor;

    public LoginFragmentViewModel(@NonNull Application application) {
        super(application);
        LoginModel.setOnUserResponseListener(this);
        connectionStateMonitor = new ConnectionStateMonitor(getApplication());
    }

    void onLoginClicked() {
        mInProgress.setValue(true);

        boolean isEmailValid = ValidateUtil.isEmailValid(mEmail.getValue());
        boolean isPasswordValid = ValidateUtil.isPasswordValid(mPassword.getValue());

        setUpEmailError(isEmailValid);
        setUpPasswordError(isPasswordValid);

        if (isPasswordValid && isEmailValid) {
            startLoadUser();
        } else {
            mInProgress.setValue(false);
        }

    }

    private void setUpEmailError(boolean isEmailValid) {
        if (!isEmailValid) {
            mEmailError.setValue(R.string.error_email);
        } else {
            mEmailError.setValue(null);
        }
    }

    private void setUpPasswordError(boolean isPasswordValid) {
        if (!isPasswordValid) {
            mPasswordError.setValue(R.string.error_password);
        } else {
            mPasswordError.setValue(null);
        }
    }

    private void startLoadUser() {
        User user = new User(mEmail.getValue(), mPassword.getValue());
        LoginModel.getUser(user);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "onCleared");
        LoginModel.clearUserListener();
    }

    /**
     * there i could handle response from API
     * it's method will be called in case of successful login
     * there i save user in database for using later
     *
     * @param user user object with needed fields
     */
    @Override
    public void onResponse(User user) {
        if (ValidateUtil.isUserValid(user)) {
            mSharedPreferenceManager.saveUser(user);

            mInProgress.setValue(false);
            mIsLogged.setValue(true);
        } else {
            mInProgress.setValue(false);
            mOnError.setValue(R.string.error_try_again);
        }
    }

    /**
     * here i could handle response from API
     * it's method will be called in case of fail login
     * there also could be system messages which will not showing
     *
     * @param toShow   is this message should be showed
     * @param errorMsg message what wrong(no such user,no internet) etc.
     */
    @Override
    public void onFailure(int errorMsg, boolean toShow) {
        mInProgress.setValue(false);
        if (toShow) {
            mOnError.setValue(errorMsg);
        }
    }

    LiveData<Integer> getEmailErrorLiveData() {
        return mEmailError;
    }

    LiveData<Integer> getPasswordErrorLiveData() {
        return mPasswordError;
    }

    LiveData<Boolean> getIsBusyLiveData() {
        return mInProgress;
    }

    LiveData<Boolean> getIsLoggedLiveData() {
        return mIsLogged;
    }

    LiveData<Integer> getOnErrorLiveData() {
        return mOnError;
    }

    LiveData<Boolean> getConnectionStateMonitor() {
        return connectionStateMonitor.getConnectionState();
    }

    void setEmail(String email) {
        mEmail.setValue(email);
    }

    void setPassword(String password) {
        mPassword.setValue(password);
    }
}