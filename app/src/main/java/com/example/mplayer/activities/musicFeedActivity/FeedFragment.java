package com.example.mplayer.activities.musicFeedActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.mplayer.R;
import com.example.mplayer.activities.pojo.Playlist;
import com.example.mplayer.adapter.TrackListAdapter;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class FeedFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener {

    public interface TrackStateListener {
        void onTrackStateChanged(Track track);
    }

    private static final String TAG = "FeedFragment";
    private static final String FRAGMENT_TYPE = "fragment_type";

    private static TrackStateListener mTrackListenerForAdapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private FeedViewModel mTrackListViewModel;

    private Constants.FEED_TYPES mCurrentFeedType;

    static FeedFragment newInstance(Constants.FEED_TYPES type) {
        FeedFragment feedFragment = new FeedFragment();
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_TYPE, type);
        feedFragment.setArguments(args);
        return feedFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCurrentFeedType = (Constants.FEED_TYPES) getArguments().getSerializable(FRAGMENT_TYPE);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tracks_feed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout = view.findViewById(R.id.refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        initializeRecycler(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mTrackListViewModel = ViewModelProviders.of(getActivity()).get(FeedViewModel.class);
        initializeDataObserves();
    }

    public static void setTrackStateListener(TrackStateListener listener) {
        mTrackListenerForAdapter = listener;
    }

    private void initializeDataObserves() {
        mTrackListViewModel.getErrorMessage()
                .observe(getViewLifecycleOwner(), errorMsg ->
                        Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show());

        mTrackListViewModel.getIsDataLoadingLiveData().observe(getViewLifecycleOwner(), isRefreshing ->
                mSwipeRefreshLayout.setRefreshing(isRefreshing));
        mTrackListViewModel.getTracksFeed().observe(getViewLifecycleOwner(), tracks -> {

            TrackListAdapter adapter = (TrackListAdapter)
                    Objects.requireNonNull(mRecyclerView.getAdapter());
            adapter.refreshData(tracks);
        });

        mTrackListViewModel.getOnTrackStateChanged().observe(getViewLifecycleOwner(), playlist -> {
            Log.d(Constants.TAG_CHAIN, "chain 5,result from viewModel to feedFragment: ");
            mTrackListenerForAdapter.onTrackStateChanged(playlist.getCurrentTrack());
        });

        mTrackListViewModel.getResetItemPosition().observe(getViewLifecycleOwner(), position ->
                ((TrackListAdapter) Objects.requireNonNull(
                        mRecyclerView.getAdapter()))
                        .updateItem(position)
        );

        mTrackListViewModel.getIsRefreshingAvailable().observe(getViewLifecycleOwner(),
                isAvailable -> mSwipeRefreshLayout.setEnabled(isAvailable));
    }

    private void initializeRecycler(View view) {
        mRecyclerView = view.findViewById(R.id.rec_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                view.getContext()
        );
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setLayoutManager(layoutManager);
        TrackListAdapter adapter = new TrackListAdapter(new ArrayList<>());
        adapter.setTrackClickListener(new TrackListAdapter.TrackClickListener() {

            @Override
            public void onPlayClicked(int previousPosition, Playlist playlist) {
                Log.d(Constants.TAG_CHAIN,
                        "chain 1,from adapter to feedFragment,onPlayClicked");

                mTrackListViewModel.onStartPlayClicked(playlist);
            }

            @Override
            public void onPauseClicked() {
                mTrackListViewModel.onPausePlayClicked();
            }

            @Override
            public void onResumeClicked() {
                mTrackListViewModel.onResumePlayClicked();
            }

        });
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "refreshing,current type:" + mCurrentFeedType.toString());
        mTrackListViewModel.feedPulledToRefresh();
    }
}