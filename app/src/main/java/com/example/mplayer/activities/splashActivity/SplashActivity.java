package com.example.mplayer.activities.splashActivity;

import android.content.Intent;
import android.os.Bundle;
import android.transition.Explode;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mplayer.activities.loginScreen.LoginActivity;
import com.example.mplayer.activities.musicFeedActivity.MainActivity;
import com.example.mplayer.util.SharedPreferencesManager;

public class SplashActivity extends AppCompatActivity {
    private static final int DURATION_OF_SPLASH_MILLIS = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SharedPreferencesManager.getInstance(getApplicationContext()).loadUser() != null) {
            startNewActivity(new Intent(this, MainActivity.class));
        } else {
            startNewActivity(new Intent(this, LoginActivity.class));
        }
    }

    /**
     * start new activity after pause defined in scope
     * with animation of explode for splash activity
     *
     * @param intent which activity to start
     */
    private void startNewActivity(Intent intent) {
        try {
            Thread.sleep(DURATION_OF_SPLASH_MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getWindow().setExitTransition(new Explode());
        startActivity(intent);
        finish();
    }
}