package com.example.mplayer.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.NotNull;

public class ConnectionStateMonitor {

    private ConnectivityManager mConnectivityManager;
    private MutableLiveData<Boolean> mConnectionState = new SingleLiveEvent<>();

    public ConnectionStateMonitor(Context context) {
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        ConnectivityManager.NetworkCallback networkCallback = new NetworkCallback();
        mConnectivityManager.registerDefaultNetworkCallback(networkCallback);
    }

    class NetworkCallback extends ConnectivityManager.NetworkCallback {

        @Override
        public void onAvailable(@NotNull Network network) {
            updateConnection();
        }

        @Override
        public void onLost(@NotNull Network network) {
            updateConnection();
        }
    }

    private void updateConnection() {
        if (mConnectivityManager != null) {
            NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
            boolean isNetworkAvailable = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            mConnectionState.postValue(isNetworkAvailable);
        }
    }

    public LiveData<Boolean> getConnectionState() {
        return mConnectionState;
    }

    public boolean isNetworkAvailable() {
        return mConnectivityManager.getActiveNetworkInfo() != null;
    }
}