package com.example.mplayer.util;

public class FormatUtil {

    private static final int SECONDS_IN_MINUTE = 60;

    public static String formatTimeSecondsToString(int duration) {
        int minutes = duration / SECONDS_IN_MINUTE;
        int seconds = duration % SECONDS_IN_MINUTE;
        return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
    }

}