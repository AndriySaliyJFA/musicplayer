package com.example.mplayer.util;

import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.database.entities.TrackRecord;

public class TrackConverter {

    public static TrackRecord TrackToRecord(Track track) {
        return new TrackRecord(track);
    }

    public static Track RecordToTrack(TrackRecord trackRecord) {
        Track track = new Track();

        track.setId(trackRecord.getId());
        track.setFeedType(trackRecord.getFeedType());
        track.setDuration(trackRecord.getDuration());
        track.setVisibility(trackRecord.getVisibility());

        track.setDescription(trackRecord.getDescription());
        track.setTitle(trackRecord.getTitle());
        track.setThumb(trackRecord.getThumb());
        track.setUri(trackRecord.getUri());
        track.setStreamUrl(trackRecord.getStreamUrl());

        track.setUser(new User(trackRecord.getAuthorName()));
        return track;
    }
}