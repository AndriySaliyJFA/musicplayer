package com.example.mplayer.util;

import com.example.mplayer.api.entities.Track;

import java.util.List;

public class TrackUtil {

    public static int findActiveItemPosition(final List<Track> trackList) {
        for (int i = 0; i < trackList.size(); i++) {
            if (!trackList.get(i).getState().equals(Track.STATE_TYPES.STATE_IDLE)) {
                return i;
            }
        }
        return -1;
    }
}