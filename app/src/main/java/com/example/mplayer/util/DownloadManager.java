package com.example.mplayer.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.example.mplayer.api.entities.Track;
import com.example.mplayer.database.DatabaseManager;

import org.jetbrains.annotations.NotNull;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadManager {

    public interface DownloadListener {
        void onDownloadStateChange(Track track);

        void onDownloadError(Track track);
    }

    public static int MAX_DURATION_IN_SECONDS = 400;
    private static final String TAG = "DownloadManager";
    private static final String FORMAT = ".mp3";
    private static final int BUFFER_SIZE = 32768;

    @SuppressLint("StaticFieldLeak")
    private static DownloadManager mInstance;
    @SuppressLint("StaticFieldLeak")
    private static Context mContext = null;
    private static DownloadListener mListener = null;
    private static String FILE_PATH;
    private static DatabaseManager mDatabaseManager = null;

    private ExecutorService executor
            = Executors.newSingleThreadExecutor();

    private DownloadManager(Context context) {
        mContext = context;
        FILE_PATH = context.getFilesDir() + "/";
    }

    public Context getContext() {
        return mContext;
    }

    public static DownloadManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DownloadManager(context.getApplicationContext());
            mDatabaseManager = DatabaseManager.getInstance(mContext);
        }
        return mInstance;
    }

    public static void setOnDownloadListener(DownloadListener listener) {
        mListener = listener;
    }

    public static boolean isDownloadListenerRegistered() {
        return mListener != null;
    }

    public void downloadTrack(Track track) {
        executor.shutdownNow();
        executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> downloadFile(track));
    }

    private void downloadFile(Track track) {
        track.setState(Track.STATE_TYPES.STATE_DOWNLOADING);
        mListener.onDownloadStateChange(track);
        if (track.getDuration() > MAX_DURATION_IN_SECONDS) {
            track.setFilePath(track.getStreamUrl());
            track.setState(Track.STATE_TYPES.STATE_DOWNLOADED);
            mListener.onDownloadStateChange(track);
            return;
        }
        if (isFileExistInDatabase(track.getId())) {
            mListener.onDownloadStateChange(getTrackPathFromDatabase(track));
            return;
        } else {
            try {
                URL url = new URL(track.getStreamUrl());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(3000);
                connection.connect();
                Log.d(TAG, "connected");

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new ConnectException("Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage());
                }

                Log.d(TAG, "start load file,filename:" + track.getId());
                InputStream input = connection.getInputStream();
                FileOutputStream fos = mContext.openFileOutput(track.getId() + ".mp3", Context.MODE_PRIVATE);
                byte[] buffer = new byte[BUFFER_SIZE];

                int len1;
                int part = 0;
                while ((len1 = input.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    if (part % 100 == 0)
                        Log.d(TAG, "id=" + Thread.currentThread().getName() + " writing part:" + part);
                    ++part;
                    if (Thread.currentThread().isInterrupted()) {
                        fos.flush();
                        fos.close();
                        input.close();
                        connection.disconnect();
                        return;
                    }
                }
                Log.d(TAG, "writing complete");

                fos.flush();
                fos.close();
                input.close();
            } catch (ConnectException e) {
                Log.e(TAG, "connection error" + e.getMessage());
                mListener.onDownloadError(track);
                return;
            } catch (MalformedURLException e) {
                Log.e(TAG, "incorrect URL" + e.getMessage());
                mListener.onDownloadError(track);
                return;
            } catch (IOException e) {
                Log.e(TAG, "no such file" + e.getMessage());
                mListener.onDownloadError(track);
                return;
            }
        }
        saveFilePathInDatabase(track);
        mListener.onDownloadStateChange(getTrackPathFromDatabase(track));
    }

    private void saveFilePathInDatabase(@NotNull Track track) {
        track.setFilePath(FILE_PATH + track.getId() + FORMAT);
        mDatabaseManager.saveTrackPathInDatabase(track.getId(), track.getFilePath());
    }

    private boolean isFileExistInDatabase(int id) {
        return mDatabaseManager.loadTrackPath(id) != null;
    }

    private Track getTrackPathFromDatabase(@NotNull Track track) {
        track.setFilePath(mDatabaseManager.loadTrackPath(track.getId()));
        track.setState(Track.STATE_TYPES.STATE_DOWNLOADED);
        return track;
    }
}