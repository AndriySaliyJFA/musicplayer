package com.example.mplayer.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.mplayer.activities.loginScreen.entities.User;
import com.google.gson.Gson;

public class SharedPreferencesManager {

    private static final String USER_KEY = "user_key";
    private static final String APP_PREFERENCES = "com.example.mplayer";

    private static SharedPreferencesManager mInstance = null;

    private static SharedPreferences sSharedPreferences;
    private static Gson sGson = new Gson();

    public static SharedPreferencesManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferencesManager(context);
        }
        return mInstance;
    }

    private SharedPreferencesManager(Context context) {
        sSharedPreferences = getSharedPreferences(context.getApplicationContext());
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void saveUser(User user) {
        String json = sGson.toJson(user);
        sSharedPreferences.edit()
                .putString(USER_KEY, json)
                .apply();
    }

    public User loadUser() {
        String json = sSharedPreferences.getString(USER_KEY, null);

        return sGson.fromJson(json, User.class);
    }

    public void deleteUser() {
        sSharedPreferences.edit()
                .remove(USER_KEY)
                .apply();
    }
}