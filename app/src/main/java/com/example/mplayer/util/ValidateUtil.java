package com.example.mplayer.util;

import android.util.Log;
import android.util.Patterns;

import com.example.mplayer.activities.loginScreen.entities.User;

public class ValidateUtil {

    private static final String TAG = "ValidateUtil";

    public static boolean isEmailValid(String email) {
        return email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(String password) {
        return password != null && password.length() > 0;
    }

    public static boolean isUserValid(User user) {
        if (user.getKey() == null) {
            Log.e(TAG, "no key field");
            return false;
        }
        if (user.getSecret() == null) {
            Log.e(TAG, "no secret field");
            return false;
        }
        if (user.getThumb() == null) {
            Log.e(TAG, "no thumb field");
            return false;
        }
        if (user.getUsername() == null) {
            Log.e(TAG, "No username fields");
            return false;
        }
        return true;
    }
}