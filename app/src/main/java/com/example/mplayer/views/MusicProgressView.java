package com.example.mplayer.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.mplayer.R;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.FormatUtil;

public class MusicProgressView extends RelativeLayout {

    public interface ProgressStateListener {
        void onProgressStateChanged(int newProgress);
    }

    private static final int DEFAULT_MAX_PROGRESS = 100;
    private static final String TAG = "MusicProgressView";

    private final TextView mCurrentTime;
    private final TextView mDurationTime;
    private final SeekBar mSeekBar;

    public MusicProgressView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.music_progress_view, this);
        mCurrentTime = findViewById(R.id.textCurrentTime);
        mDurationTime = findViewById(R.id.textDurationTime);
        mSeekBar = findViewById(R.id.seekBar);
        mSeekBar.setMax(DEFAULT_MAX_PROGRESS);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth() + getPaddingStart() + getPaddingEnd();
        int height = getMeasuredHeight() + getPaddingTop() + getPaddingBottom();

        setMeasuredDimension(width, height);
    }

    public void restoreState(Track currentTrack) {
        mSeekBar.setMax(currentTrack.getDuration());
        mDurationTime.setText(FormatUtil.formatTimeSecondsToString(currentTrack.getDuration()));
        mCurrentTime.setText(FormatUtil.formatTimeSecondsToString(mSeekBar.getProgress()));
    }

    public void setOnProgressChangeListener(ProgressStateListener progressStateListener) {
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int prevProgress;

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                prevProgress = seekBar.getProgress();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (prevProgress != seekBar.getProgress()) {
                    progressStateListener.onProgressStateChanged(seekBar.getProgress());
                }
            }
        });
    }

    public void changeTrack(Track track) {
        mDurationTime.setText(FormatUtil.formatTimeSecondsToString(track.getDuration()));
        mCurrentTime.setText(FormatUtil.formatTimeSecondsToString(0));
        mSeekBar.setMax(track.getDuration());
        mSeekBar.setProgress(0);
    }

    public void dropProgress() {
        mSeekBar.setProgress(0);
        mCurrentTime.setText(FormatUtil.formatTimeSecondsToString(0));
    }

    public void updateProgressBy(int i) {
        int currentTime = mSeekBar.getProgress() + i;

        if (currentTime <= mSeekBar.getMax()) {
            mCurrentTime.setText(FormatUtil.formatTimeSecondsToString(currentTime));
            mSeekBar.setProgress(currentTime);
        } else {
            Log.e(TAG, "updateProgress: error,progress greater than duration!!");
        }
    }
}