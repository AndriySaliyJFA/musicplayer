package com.example.mplayer.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.StringDef;
import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.NotificationTarget;
import com.example.mplayer.R;
import com.example.mplayer.activities.pojo.Playlist;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.TrackUtil;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static com.example.mplayer.services.MusicPlayerService.ACTION_TYPE.ACTION_NEXT;
import static com.example.mplayer.services.MusicPlayerService.ACTION_TYPE.ACTION_PLAY;
import static com.example.mplayer.services.MusicPlayerService.ACTION_TYPE.ACTION_PREVIOUS;
import static com.example.mplayer.services.MusicPlayerService.ACTION_TYPE.ACTION_STOP_FOREGROUND_SERVICE;

public class MusicPlayerService extends Service {

    public interface MusicPLayerStateListener {

        void onPlayClicked(Playlist playlist);

        void onNextClicked(Playlist playlist);

        void onPreviousClicked(Playlist playlist);
    }

    @StringDef({ACTION_STOP_FOREGROUND_SERVICE, ACTION_PLAY, ACTION_NEXT, ACTION_PREVIOUS})
    @interface ACTION_TYPE {
        String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
        String ACTION_PLAY = "action_play";
        String ACTION_NEXT = "action_next";
        String ACTION_PREVIOUS = "action_previous";
    }

    private static final String TAG = "MusicPlayerService";
    private static final String CHANEL_PLAYER = "chanel_player";

    private Notification mNotification;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    private RemoteViews mRemoteViews;

    private final IBinder mLocalBinder = new LocalBinder();
    private MusicPLayerStateListener mStateListener;
    private MediaPlayer mPlayer;

    private Playlist mPlaylist;
    private Track mCurrentTrack;

    private int mNotificationId = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        mPlayer = new MediaPlayer();
        mRemoteViews = new RemoteViews(getPackageName(), R.layout.notification_player);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotification = createNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            switch (Objects.requireNonNull(intent.getAction())) {
                case ACTION_STOP_FOREGROUND_SERVICE:
                    stopPlay();
                    break;
                case ACTION_PLAY:
                    if (mPlayer.isPlaying()) {
                        pausePlay();
                    } else {
                        resumePlay();
                    }
                    break;
                case ACTION_NEXT:
                    mPlayer.stop();
                    updateNotification(Track.STATE_TYPES.STATE_DOWNLOADING);
                    mStateListener.onNextClicked(mPlaylist);
                    break;
                case ACTION_PREVIOUS:
                    mPlayer.stop();
                    updateNotification(Track.STATE_TYPES.STATE_DOWNLOADING);
                    mStateListener.onPreviousClicked(mPlaylist);
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void startForegroundService() {
        Log.d(TAG, "startForegroundService");
        startForeground(mNotificationId, mNotification);
    }

    public void updatePlaylist(List<Track> tracks) {
        int position = TrackUtil.findActiveItemPosition(tracks);
        if (position != -1) {
            mCurrentTrack = tracks.get(position);
            mPlaylist = new Playlist(tracks, position);
        }
    }

    public void startPlay(Track track, Playlist playlist) {
        Log.d(TAG, "startPlay: track name" + track.getTitle());
        try {
            mPlaylist = playlist;
            mCurrentTrack = track;
            mPlayer.stop();
            mPlayer.reset();

            updateNotification(Track.STATE_TYPES.STATE_DOWNLOADING);

            if (mCurrentTrack.getFilePath() != null) {
                mPlayer.setDataSource(mCurrentTrack.getFilePath());
                mPlayer.prepareAsync();
                mPlayer.setOnPreparedListener(mediaPlayer -> {
                    mediaPlayer.start();
                    updateNotification(Track.STATE_TYPES.STATE_PLAYING);

                    mCurrentTrack.setState(Track.STATE_TYPES.STATE_PLAYING);
                    playlist.setCurrentTrack(mCurrentTrack);
                    mStateListener.onPlayClicked(playlist);
                });

                mPlayer.setOnCompletionListener(mediaPlayer -> {
                    mPlayer.stop();
                    mStateListener.onNextClicked(playlist);
                });
            } else {
                Log.e(TAG, "startPlay ,path=null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pausePlay() {
        Log.d(TAG, "pause play");
        if (mCurrentTrack != null) {
            mPlayer.pause();
            updateNotification(Track.STATE_TYPES.STATE_PAUSED);

            mCurrentTrack.setState(Track.STATE_TYPES.STATE_PAUSED);
            mStateListener.onPlayClicked(mPlaylist.setCurrentTrack(mCurrentTrack));
        }
    }

    public void stopPlay() {
        Log.d(TAG, "stopPlay");
        mPlayer.stop();
        mCurrentTrack.setState(Track.STATE_TYPES.STATE_IDLE);
        mStateListener.onPlayClicked(mPlaylist.setCurrentTrack(mCurrentTrack));

        stopForeground(true);

        //mNotificationManager.cancel(mNotificationId);
        //stopForeground(0);

    }

    public void resumePlay() {
        Log.d(TAG, "resumePlay");
        if (mCurrentTrack != null) {
            mPlayer.start();
            updateNotification(Track.STATE_TYPES.STATE_PLAYING);

            mCurrentTrack.setState(Track.STATE_TYPES.STATE_PLAYING);
            mStateListener.onPlayClicked(mPlaylist.setCurrentTrack(mCurrentTrack));
        }
    }

    void onProgressChanged(int newProgress) {
        mPlayer.seekTo(newProgress * 1000);
    }

    private void updateNotification(String state) {
        switch (state) {
            case Track.STATE_TYPES.STATE_DOWNLOADING:
                mRemoteViews.setImageViewResource(R.id.btn_notify_play, R.drawable.progress_rotate);
                break;
            case Track.STATE_TYPES.STATE_PAUSED:
                mRemoteViews.setImageViewResource(R.id.btn_notify_play, R.drawable.play);
                break;
            case Track.STATE_TYPES.STATE_PLAYING:
                mRemoteViews.setImageViewResource(R.id.btn_notify_play, R.drawable.pause);
                break;
        }

        setUpNotificationImage();

        mRemoteViews.setTextViewText(R.id.tv_notify_track_name, mCurrentTrack.getTitle());

        mNotificationManager.notify(mNotificationId, rebuildNotification());
    }

    private void setUpNotificationImage() {
        NotificationTarget notificationTarget = new NotificationTarget(
                getApplicationContext(),
                R.id.iv_notify_thumb,
                mRemoteViews,
                mBuilder.build(),
                mNotificationId);

        Glide.with(getApplicationContext())
                .asBitmap()
                .load(mCurrentTrack.getThumb())
                .into(notificationTarget);
    }

    private Notification rebuildNotification() {
        return mBuilder.setContent(mRemoteViews).build();
    }

    private Notification createNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getPackageName(),
                    CHANEL_PLAYER, NotificationManager.IMPORTANCE_LOW);
            mNotificationManager.createNotificationChannel(channel);
        }

        Intent playIntent;
        Intent nextIntent;
        Intent previousIntent;
        Intent closeIntent;

        playIntent = new Intent(this, MusicPlayerService.class);
        playIntent.setAction(ACTION_PLAY);

        nextIntent = new Intent(this, MusicPlayerService.class);
        nextIntent.setAction(ACTION_NEXT);

        previousIntent = new Intent(this, MusicPlayerService.class);
        previousIntent.setAction(ACTION_PREVIOUS);

        closeIntent = new Intent(this, MusicPlayerService.class);
        closeIntent.setAction(ACTION_STOP_FOREGROUND_SERVICE);

        PendingIntent pendingPlayIntent = PendingIntent.getService(this, 0, playIntent, 0);
        PendingIntent pendingNextIntent = PendingIntent.getService(this, 0, nextIntent, 0);
        PendingIntent pendingPreviousIntent = PendingIntent.getService(this, 0, previousIntent, 0);
        PendingIntent pendingCloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);

        mRemoteViews.setTextViewText(R.id.tv_notify_track_name, getText(R.string.default_song));

        mRemoteViews.setOnClickPendingIntent(R.id.btn_notify_play, pendingPlayIntent);
        mRemoteViews.setOnClickPendingIntent(R.id.btn_notify_next, pendingNextIntent);
        mRemoteViews.setOnClickPendingIntent(R.id.btn_notify_prev, pendingPreviousIntent);
        mRemoteViews.setOnClickPendingIntent(R.id.btn_notify_close, pendingCloseIntent);

        mRemoteViews.setImageViewResource(R.id.btn_notify_play, R.drawable.pause);

        mBuilder = new NotificationCompat.Builder(this, getPackageName());
        mBuilder.setSmallIcon(R.drawable.music_icon)
                .setOngoing(true)
                .setContent(mRemoteViews);

        return mBuilder.build();
    }

    public Playlist getSavedPlaylist() {
        return mPlaylist;
    }

    public void setMusicPlayerStateListener(MusicPLayerStateListener listener) {
        mStateListener = listener;
    }

    public class LocalBinder extends Binder {
        public MusicPlayerService getService() {
            return MusicPlayerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mLocalBinder;
    }
}