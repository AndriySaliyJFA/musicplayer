package com.example.mplayer.services;

import android.util.Log;

import com.example.mplayer.R;
import com.example.mplayer.activities.pojo.Playlist;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.Constants;
import com.example.mplayer.util.DownloadManager;

import java.util.List;

public class MusicPlayerServiceManager {

    public interface ServiceStateListener {
        void onStateChanged(Playlist playlist);

        void onServiceError(int messageStringId);
    }

    private static MusicPlayerServiceManager sMusicPlayerServiceManager;

    private Playlist mPlaylist;
    private DownloadManager mDownloadManager;
    private MusicPlayerService mMusicPlayerService;

    private ServiceStateListener mServiceStateListener;

    private MusicPlayerServiceManager(MusicPlayerService musicPlayerService,
                                      DownloadManager downloadManager) {
        mMusicPlayerService = musicPlayerService;
        mDownloadManager = downloadManager;
        initializeListeners();
    }

    public static MusicPlayerServiceManager newInstance(MusicPlayerService musicPlayerService,
                                                        DownloadManager downloadManager) {
        if (sMusicPlayerServiceManager == null) {
            sMusicPlayerServiceManager = new MusicPlayerServiceManager(musicPlayerService, downloadManager);
        }
        return sMusicPlayerServiceManager;
    }

    public static MusicPlayerServiceManager getInstance() {
        return sMusicPlayerServiceManager;
    }

    public Playlist getSavedData() {
        mPlaylist = mMusicPlayerService.getSavedPlaylist();
        if (mPlaylist != null) {
            return mPlaylist;
        } else {
            return new Playlist();
        }
    }

    public void updateSavedData(List<Track> tracks) {
        mMusicPlayerService.updatePlaylist(tracks);
    }

    public void onProgressChanged(int newProgress) {
        mMusicPlayerService.onProgressChanged(newProgress);
    }

    private void downloadTrack(Track track) {
        if (!DownloadManager.isDownloadListenerRegistered()) {
            DownloadManager.setOnDownloadListener(new DownloadManager.DownloadListener() {
                @Override
                public void onDownloadStateChange(Track track) {
                    Log.d(
                            Constants.TAG_CHAIN,
                            ":chain 3, from downloadManager to Service manager  "
                                    + track.getState()
                    );

                    String state = track.getState();
                    switch (state) {
                        case Track.STATE_TYPES.STATE_DOWNLOADED:
                            Log.d("chain", ":chain 3 downloaded ended,  ");
                            mMusicPlayerService.startPlay(track, mPlaylist);
                            break;
                        case Track.STATE_TYPES.STATE_IDLE:
                        case Track.STATE_TYPES.STATE_PLAYING:
                        case Track.STATE_TYPES.STATE_PAUSED:
                        case Track.STATE_TYPES.STATE_DOWNLOADING:
                            mServiceStateListener.onStateChanged(mPlaylist);
                            break;
                        default:
                            throw new RuntimeException("wrong state type!");
                    }
                }

                @Override
                public void onDownloadError(Track track) {
                    track.setState(Track.STATE_TYPES.STATE_IDLE);
                    mServiceStateListener.onStateChanged(mPlaylist);
                    mServiceStateListener.onServiceError(R.string.error_try_again);
                }
            });
        }
        mDownloadManager.downloadTrack(track);
    }

    public void startPlay(Playlist playlist) {
        Track track = playlist.getCurrentTrack();
        mPlaylist = playlist;
        mMusicPlayerService.startForegroundService();

        if (track.getDuration() < DownloadManager.MAX_DURATION_IN_SECONDS) {
            downloadTrack(track);
        } else {
            mServiceStateListener.onServiceError(R.string.error_caching_too_large);
            downloadTrack(track);
        }
    }

    public void pausePlay() {
        mMusicPlayerService.pausePlay();
    }

    public void resumePlay() {
        mMusicPlayerService.resumePlay();
    }

    private void initializeListeners() {
        MusicPlayerService.MusicPLayerStateListener musicPlayerListener =
                new MusicPlayerService.MusicPLayerStateListener() {
                    @Override
                    public void onPlayClicked(Playlist playlist) {
                        mServiceStateListener.onStateChanged(playlist);
                    }

                    @Override
                    public void onNextClicked(Playlist playlist) {
                        playlist.nextTrack();
                        startPlay(playlist);
                    }

                    @Override
                    public void onPreviousClicked(Playlist playlist) {
                        playlist.previousTrack();
                        startPlay(playlist);
                    }
                };
        mMusicPlayerService.setMusicPlayerStateListener(musicPlayerListener);

    }

    public void setServiceStateListener(ServiceStateListener serviceStateListener) {
        mServiceStateListener = serviceStateListener;
    }
}