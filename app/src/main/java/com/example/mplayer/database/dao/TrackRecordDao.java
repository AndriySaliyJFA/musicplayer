package com.example.mplayer.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.mplayer.database.entities.TrackRecord;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface TrackRecordDao {

    @Query("SELECT * FROM trackRecords WHERE mFeedType = :feedType")
    List<TrackRecord> getByType(int feedType);

    @Insert(onConflict = REPLACE)
    void insertTracks(List<TrackRecord> tracks);

    @Query("DELETE FROM trackRecords WHERE mFeedType =:feedType")
    void clearByType(int feedType);
}