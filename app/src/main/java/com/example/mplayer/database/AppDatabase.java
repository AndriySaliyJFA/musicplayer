package com.example.mplayer.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.mplayer.database.dao.TrackPathDao;
import com.example.mplayer.database.dao.TrackRecordDao;
import com.example.mplayer.database.entities.TrackPath;
import com.example.mplayer.database.entities.TrackRecord;

@Database(entities = {TrackRecord.class, TrackPath.class}, version = 1, exportSchema = false)
abstract class AppDatabase extends RoomDatabase {

    abstract TrackRecordDao mTrackRecordDao();

    abstract TrackPathDao mTrackPathDao();
}