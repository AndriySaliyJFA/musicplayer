package com.example.mplayer.database;

import android.content.Context;

import androidx.room.Room;

import com.example.mplayer.api.entities.Track;
import com.example.mplayer.database.dao.TrackPathDao;
import com.example.mplayer.database.dao.TrackRecordDao;
import com.example.mplayer.database.entities.TrackPath;
import com.example.mplayer.database.entities.TrackRecord;
import com.example.mplayer.util.Constants;
import com.example.mplayer.util.TrackConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DatabaseManager {

    private static final int TYPE_NEW = 0;
    private static final int TYPE_POPULAR = 1;

    private static DatabaseManager sInstance = null;

    private TrackPathDao mTrackPathDao;
    private TrackRecordDao mTrackRecordDao;

    private List<TrackRecord> mTrackRecords = new ArrayList<>();
    private List<Track> mTracks = new ArrayList<>();

    private ExecutorService mExecutorService = Executors.newSingleThreadExecutor();

    public static DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context);
        }
        return sInstance;
    }

    private DatabaseManager(Context context) {
        AppDatabase database = Room.databaseBuilder(context,
                AppDatabase.class, "database").build();
        mTrackPathDao = database.mTrackPathDao();
        mTrackRecordDao = database.mTrackRecordDao();
    }

    public void saveFeedInDatabase(List<Track> trackList, Constants.FEED_TYPES feedType) {
        mTrackRecords.clear();

        for (Track track : trackList) {
            track.setFeedType(feedType);
            mTrackRecords.add(TrackConverter.TrackToRecord(track));
        }

        Runnable runnable = () -> {
            mTrackRecordDao.clearByType(trackList.get(0).getFeedType());
            mTrackRecordDao.insertTracks(mTrackRecords);
        };
        mExecutorService.submit(runnable);
    }

    public List<Track> loadFeedFromDatabase(Constants.FEED_TYPES feedType) {
        int type = feedType == Constants.FEED_TYPES.TYPE_NEW ? TYPE_NEW : TYPE_POPULAR;

        Callable<List<Track>> callableTask = () -> {
            List<TrackRecord> trackRecords = mTrackRecordDao.getByType(type);
            mTracks.clear();
            for (TrackRecord trackRecord : trackRecords) {
                mTracks.add(TrackConverter.RecordToTrack(trackRecord));
            }
            return mTracks;
        };
        Future<List<Track>> result = mExecutorService.submit(callableTask);
        try {
            return result.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveTrackPathInDatabase(int id, String filePath) {
        Runnable runnable = () ->
                mTrackPathDao.insertTrack(new TrackPath(id, filePath));
        mExecutorService.submit(runnable);
    }

    public String loadTrackPath(int id) {
        Callable<String> callableTask = () -> mTrackPathDao.getById(id).getFilePath();

        Future<String> result = mExecutorService.submit(callableTask);
        try {
            return result.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}