package com.example.mplayer.database.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.mplayer.api.entities.Track;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@Entity(tableName = "trackRecords")
public class TrackRecord {
    @PrimaryKey(autoGenerate = true)

    private int mId;
    private int mDuration;
    private int mVisibility;
    private int mFeedType;

    private String mDescription;
    private String mTitle;
    private String mThumb;
    private String mUri;
    private String mStreamUrl;
    private String mAuthorName;

    public TrackRecord() {
    }

    public TrackRecord(Track track) {
        mId = track.getId();
        mDuration = track.getDuration();
        mVisibility = track.getVisibility();
        mFeedType = track.getFeedType();

        mDescription = track.getDescription();
        mTitle = track.getTitle();
        mThumb = track.getThumb();
        mUri = track.getUri();
        mStreamUrl = track.getStreamUrl();
        mAuthorName = track.getUser().getUsername();
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        this.mDuration = duration;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getThumb() {
        return mThumb;
    }

    public void setThumb(String thumb) {
        this.mThumb = thumb;
    }

    public String getStreamUrl() {
        return mStreamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.mStreamUrl = streamUrl;
    }

    public String getUri() {
        return mUri;
    }

    public void setUri(String uri) {
        this.mUri = uri;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public void setVisibility(int visibility) {
        this.mVisibility = visibility;
    }

    public int getVisibility() {
        return mVisibility;
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public void setAuthorName(String name) {
        this.mAuthorName = name;
    }

    public int getFeedType() {
        return mFeedType;
    }

    public void setFeedType(int feedType) {
        mFeedType = feedType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrackRecord that = (TrackRecord) o;
        return mId == that.mId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mId);
    }

    @NotNull
    @Override
    public String toString() {
        return "Track{" +
                "mId=" + mId +
                ", mDuration=" + mDuration +
                ", mTitle='" + mTitle + '\'' +
                ", mThumb='" + mThumb + '\'' +
                ", mStreamUrl='" + mStreamUrl + '\'' +
                '}';
    }
}