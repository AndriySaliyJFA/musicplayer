package com.example.mplayer.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.mplayer.database.entities.TrackPath;

@Dao
public interface TrackPathDao {

    @Query("SELECT * FROM trackPath WHERE mId = :id")
    TrackPath getById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTrack(TrackPath trackPath);
}