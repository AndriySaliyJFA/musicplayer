package com.example.mplayer.database.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TrackPath {
    @PrimaryKey
    private int mId;
    private String mFilePath;

    public TrackPath(int id, String filePath) {
        mId = id;
        mFilePath = filePath;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFilePath() {
        return mFilePath;
    }
}