package com.example.mplayer;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.api.MusicApiClient;
import com.example.mplayer.api.entities.Track;
import com.example.mplayer.util.Constants;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ApiInstrumentedTest {

    @Test
    public void getUser_responseNotNull() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().
                getUser("tehnicyss@gmail.com", "badAPI").enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                assertNotNull(response);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUser_responseBodyNotEmpty() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().
                getUser("tehnicyss@gmail.com", "badAPI").enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                assertTrue(response.isSuccessful());
                assert response.body() != null;
                assertTrue(response.body().getUsername().length() > 0);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUser_callNotNull() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().
                getUser("tehnicyss@gmail.com", "badAPI").enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                assertNotNull(call);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                assertNotNull(call);
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUser_noSuchUser() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().
                getUser("no_such_user@gmail.com", "no_such_user").enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {

                assertNull(response.body().getUsername());
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                assertNotNull(call);
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getPopular_responseNotNull() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().getFeed(Constants.FEED_TITLES.FEED_POPULAR, 1, 5).enqueue(new Callback<List<Track>>() {
            @Override
            public void onResponse(@NotNull Call<List<Track>> call, @NotNull Response<List<Track>> response) {

                assertNotNull(response);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<List<Track>> call, @NotNull Throwable t) {
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getPopular_callNotNull() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().getFeed(Constants.FEED_TITLES.FEED_POPULAR, 1, 5).enqueue(new Callback<List<Track>>() {
            @Override
            public void onResponse(@NotNull Call<List<Track>> call, @NotNull Response<List<Track>> response) {

                assertNotNull(call);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<List<Track>> call, @NotNull Throwable t) {
                assertNotNull(call);
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getNew_responseNotNull() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().getFeed(Constants.FEED_TITLES.FEED_NEW, 1, 5).enqueue(new Callback<List<Track>>() {
            @Override
            public void onResponse(@NotNull Call<List<Track>> call, @NotNull Response<List<Track>> response) {

                assertNotNull(response);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<List<Track>> call, @NotNull Throwable t) {
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getNew_callNotNull() {
        CountDownLatch latch = new CountDownLatch(1);
        MusicApiClient.getInstance().getFeed(Constants.FEED_TITLES.FEED_NEW, 1, 5).enqueue(new Callback<List<Track>>() {
            @Override
            public void onResponse(@NotNull Call<List<Track>> call, @NotNull Response<List<Track>> response) {

                assertNotNull(call);
                latch.countDown();
            }

            @Override
            public void onFailure(@NotNull Call<List<Track>> call, @NotNull Throwable t) {
                assertNotNull(call);
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}