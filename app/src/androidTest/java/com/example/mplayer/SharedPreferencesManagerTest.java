package com.example.mplayer;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.mplayer.activities.loginScreen.entities.User;
import com.example.mplayer.util.SharedPreferencesManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class SharedPreferencesManagerTest {
    private SharedPreferencesManager mPreferencesManager;
    private static User testUser;

    static {
        testUser = new User();
        testUser.setKey("tester");
        testUser.setSecret("tester");
        testUser.setUsername("tester");
        testUser.setThumb("tester");
    }

    @Before
    public void setup() {
        mPreferencesManager = SharedPreferencesManager.getInstance(
                ApplicationProvider.getApplicationContext());
    }

    @Test
    public void loadUser_afterUserSaved() {
        mPreferencesManager.saveUser(testUser);
        assertNotNull(mPreferencesManager.loadUser());
    }

    @Test
    public void loadUser_afterUserDeleted() {
        mPreferencesManager.saveUser(testUser);
        mPreferencesManager.deleteUser();
        assertNull(mPreferencesManager.loadUser());
    }
}